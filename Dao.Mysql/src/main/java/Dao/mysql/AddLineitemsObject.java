/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Model.LineItems;

/**
 *
 * @author bas200175
 */
public class AddLineitemsObject {
   private  LineItems[] l;
    private double sum;
    private double profit;

    public AddLineitemsObject(LineItems[] l, double sum, double profit) {
        this.l = l;
        this.sum = sum;
        this.profit = profit;
    }

    public LineItems[] getL() {
        return l;
    }

    public void setL(LineItems[] l) {
        this.l = l;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "AddLineitemsObject{" + "l=" + l + ", sum=" + sum + ", profit=" + profit + '}';
    }
    
    
}
