/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Dao.BussinesslLogic.DaoCustomer;
import Dao.BussinesslLogic.DaoOrders;
import Dao.BussinesslLogic.DaoPayment;
import Dao.BussinesslLogic.DaoStatistics;
import Dao.BussinesslLogic.KatabookObjectDao;

/**
 *
 * @author bas200175
 */
public class KatabookObjectImplementation implements KatabookObjectDao{

    @Override
    public DaoCustomer getDaoCustomerObject() {
        return new DaoCustomerImplementation();
    }

    @Override
    public DaoOrders getDaoOrdersObject() {
        return new DaoOrdersImplementation();
    }

    @Override
    public DaoPayment getDaoPaymentObject() {
        return new DaoPaymentImplementation();
    }

    @Override
    public DaoStatistics getDaoStatistics() {
        return new DaoStatisticImplementatation();
    }

    @Override
    public CustomerService getCustomerServiceObject() {
       return new CustomerService();
    }

    @Override
    public OrderMaintenance getOrderMaintenanceObject() {
       return new OrderMaintenance();
    }

    @Override
    public ProductMaintenance getProductMaintenanceObject() {
        return new ProductMaintenance();
    }

    @Override
    public PaymentMaintenance getPaymentMaintenanceObject() {
        return new PaymentMaintenance();
    }
    
}
