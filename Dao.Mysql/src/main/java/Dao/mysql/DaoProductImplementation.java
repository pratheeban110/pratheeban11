/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Dao.BussinesslLogic.DaoProduct;
import Model.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author bas200175
 */
public class DaoProductImplementation implements DaoProduct {

    @Override
    public void addProduct() {
        String productInsert = "insert into  cust values(?,?,?,?,?,?,?)";
        ProductMaintenance createObject = new ProductMaintenance();
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(productInsert);) {
            Product p = createObject.getProductObject();
            pst.setInt(1, p.getProducId());
            pst.setString(2, p.getProductName());
            pst.setDouble(4, p.getProductPrice());
            pst.setDouble(5, p.getProductSellingPrice());
            pst.setInt(3, p.getProductQuantity());
            pst.execute();
            System.out.println("product added sucessfully");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void viewProduct() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        String query = "select * from inventory";
        String query2 = "select * from inventory where name=?";
        ResultSet r;
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;

            } catch (IOException nfe1) {
                System.out.println("enter the correct option exception");
                System.out.println("enter correct detail");
                // nfe1.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            }

        }
        switch (view) {

            case 1 -> {

                while (true) {

                    try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query2);) {
                        System.out.println("enter the name");
                        String name = br.readLine();
                        pst.setString(1, name);
                        r = pst.executeQuery();
                        System.err.println("    *************Customer Detail************   ");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        while (r.next()) {
                            Product c = new Product(r.getInt(1), r.getString(2), r.getDouble(4), r.getDouble(5), r.getInt(3));
                            System.out.println(c);
                            System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        }
                        break;
                    } catch (IOException ex) {
                        System.out.println("exception occurs");
                        System.out.println("enter the correct detail");

                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            }

            case 2 -> {
                try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query);) {
                    r = pst.executeQuery();

                    System.err.println("    *************Customer Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    while (r.next()) {
                        Product c = new Product(r.getInt(1), r.getString(2), r.getDouble(4), r.getDouble(5), r.getInt(3));
                        System.out.println(c);
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");

                    }
                    System.out.println("");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            default ->
                System.out.println("enter the correct option");
        }

    }

    @Override
    public void updateProduct() {
        ProductMaintenance createObject = new ProductMaintenance();
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ResultSet r;
        Product p = null;
        String query = "update  inventory set quantity=? where productid=?";
        while (true) {
            try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(query); PreparedStatement pst1 = connection.prepareCall("select * from inventory where productid=?");) {
                r = pst1.executeQuery();
                while (r.next()) {
                    p = new Product(r.getInt(1), r.getString(2), r.getDouble(4), r.getDouble(5), r.getInt(3));
                }
                Product update = createObject.getProductObject();
                System.out.println("enter the product id");
                int id = Integer.parseInt(br.readLine());
                pst.setInt(2, id);
                pst.setInt(1, update.getProductQuantity());
                pst.execute();
                System.out.println("product updated");
                break;

            } catch (IOException ex) {
                System.out.println("exception occurs");
                ex.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("Numberformat exception occurs");
                System.out.println("enter correct detail");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

}
