/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Model.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bas200175
 */
public class ProductMaintenance {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public Product getProductObject() {
        Product p = null;

        try {
            while (true) {
                int productId = 0;
                System.out.println("enter the product name");
                String productName = br.readLine();
                System.out.println("enter the product price");
                float productPrice = Float.parseFloat(br.readLine());
                System.out.println("enter the product selling price");
                float sellingPrice = Float.parseFloat(br.readLine());
                System.out.println("enter the quantity");
                int productQuantity = Integer.parseInt(br.readLine());
                p = new Product(productId, productName, productPrice, sellingPrice, productQuantity);
                System.out.println("product added sucessfully");
                break;
            }
        } catch (IOException ex) {
            System.out.println("IOexception occurs");
            System.out.println("re enter correct detail");
        } catch (NumberFormatException nfe) {
            System.out.println("Number format exception");
            System.out.println("re enter correct detail");

        }

        return p;
    }

    public Product updateProductObject(Product product) {

        System.out.println("enter the product quantity to change");
        int qnty=0;
        while (true) {
            try {
                 qnty = Integer.valueOf(br.readLine());
                break;
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        product.setProductQuantity(qnty);
        return product;
    }

}
