
package Dao.mysql;

import Dao.BussinesslLogic.DaoOrders;
import Model.LineItems;
import Model.Orders;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


public class DaoOrdersImplementation implements DaoOrders {

    @Override
    public void addOrders(Orders o) {
        String orderInsert = "insert into  cust values(?,?,?,?,?,?,?)";
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        try (Connection connection = database.CreateConnection(); PreparedStatement pst1 = connection.prepareCall(orderInsert)) {

            pst1.setInt(4, o.getCusId());
            pst1.setInt(1, o.getOrderId());
            pst1.setDate(2, Date.valueOf(o.getDate()));
            pst1.setDouble(3, o.getOrderAmount());
            pst1.execute();
            addLineItems(o.getOrderId(), o.getCusId());

            System.out.println("order add sucess fully");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void addLineItems(int ordId, int cusId) {
        OrderMaintenance createObject = new OrderMaintenance();
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        try (Connection connection = database.CreateConnection(); PreparedStatement pst1 = connection.prepareStatement("insert into katabooklineitems values(?,?,?,?,?)");) {
            AddLineitemsObject lineobject = createObject.getLineItemObject(connection, ordId, cusId);
            LineItems[] l = lineobject.getL();
            double sum = lineobject.getSum();
            double profit = lineobject.getProfit();
            UpdateOrderTotal upo = new UpdateOrderTotal();
            for (int i = 0; i < l.length; i++) {
                int order = l[i].getOrderId();
                pst1.setInt(1, order);
                int proid = l[i].getProductId();
                pst1.setInt(2, proid);
                int qan = l[i].getQuantity();
                pst1.setInt(3, qan);
                double totalprice = l[i].getTotalprice();
                pst1.setDouble(4, totalprice);
                double pro = l[i].getProfit();
                pst1.setDouble(5, pro);
                pst1.execute();
                System.out.println(sum + "total+all line items");
                upo.updatetotal(connection, sum, profit, ordId);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void viewOrders() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        ResultSet r;
        String query = "select * from inventory where productid=?";
        String query1 = "select * from inventory";
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;
            } catch (IOException ex) {
                System.out.println("io exception occurs");
            } catch (NumberFormatException nfe) {
                System.out.println("Number format ecxeption occur enter correct detail");
            }
        }
        switch (view) {
            case 1 -> {

                System.out.println("enter the product Id");
                int id = 0;
                while (true) {
                    try {

                        id = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOexception occurs");
                    } catch (NumberFormatException nfe) {
                        System.out.println("number format exception occurs enter correct detail");
                    }
                }
                try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(query);) {
                    pst.setInt(1, id);

                    r = pst.executeQuery();

                    System.err.println("    *************order Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|", "Id", "orderDate", "orderAmount", "profit", "customerId");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    while (r.next()) {
                        Orders o = new Orders(r.getInt(1), r.getInt(4), LocalDate.parse(r.getString(2)), r.getDouble(3), r.getDouble(5));
                        System.out.println(o);
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }

            case 2 -> {
                try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(query1);) {
                    r = pst.executeQuery();

                    System.err.println("    *************order Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|", "Id", "orderDate", "orderAmount", "profit", "customerId");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    while (r.next()) {
                        Orders o = new Orders(r.getInt(1), r.getInt(4), LocalDate.parse(r.getString(2)), r.getDouble(3), r.getDouble(5));
                        System.out.println(o);
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(4) + "+");
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            default ->
                System.err.println("enter the correct option");
        }

    }

    @Override
    public int getCustId() {
        CreateId c = new CreateId();
        String query = "select * from cust where phone=?";
        ResultSet r;
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        int cusId = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {

            System.out.println("enter the phone number to add orders");
            String phoneNo2 = null;
            while (true) {
                try {
                    phoneNo2 = br.readLine();
                    pst.setString(1, phoneNo2);
                    r = pst.executeQuery();
                    break;
                } catch (IOException ex) {
                    System.out.println("IOException occurs");

                } catch (NullPointerException npe) {
                    System.out.println("null pointer exception ");
                    System.out.println("enter detai again");
                }
            }
            r.absolute(1);
            cusId = r.getInt(1);
             

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cusId;

    }
}
