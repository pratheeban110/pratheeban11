/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Model.Customer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bas200175
 */
public class CustomerService {

    public Customer getCustomerObject() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        Customer c = null;
        try {
            while (true) {
                int cusId = 0;
                System.out.println("enter the name");
                String name = br.readLine();
                System.out.println("enter the phone number");
                String phoneNumber = br.readLine();
                System.out.println("enter the address");
                String address = br.readLine();
                double balance = 0;
                String status = "active";
                String balanceStatus = "not_completed";
                System.out.println("customer object created sucessfully");
                c = new Customer(cusId, name, phoneNumber, address, balance, status, balanceStatus);
                break;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return c;

    }

    public Customer updateCustomerName() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Customer customer = new Customer();

        while (true) {

            try {

                System.out.println("enter the phone number to update");
                String phno = br.readLine();
                customer.setPhoneNumber(phno);
                System.out.println("enter the new name");
                String name = br.readLine();
                customer.setName(name);
                return customer;

            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return customer;

        }

    }

    public Customer updateCustomerAddress() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Customer customer = new Customer();

        while (true) {
            try {
                System.out.println("enter the phone number to update");
                String phno = br.readLine();
                customer.setPhoneNumber(phno);
                System.out.println("enter the address");
                String address = br.readLine();
                customer.setAddress(address);
                return customer;

            } catch (IOException ex) {
                ex.printStackTrace();
            }

            return customer;

        }

    }

    public Customer updateCustomerStatus() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Customer customer = new Customer();

        while (true) {
            try {

                System.out.println("enter the phone number to update");
                String phno = br.readLine();
                customer.setPhoneNumber(phno);
                String status = "nonactive";
                customer.setStatus(status);
                System.out.println("sucessfully deleted......");
                return customer;
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    
}
    
