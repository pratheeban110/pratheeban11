/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Dao.BussinesslLogic.DaoPayment;
import Model.Payment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author bas200175
 */
public class DaoPaymentImplementation implements DaoPayment {

    @Override
    public void addPayment(Payment pm) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        UpdateCustomerBalance ucb = new UpdateCustomerBalance();
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        
        String query = "select Balance,cus_id from cust where phone=?";
        String insertQuery = "insert into payment values(?,?,?,default)";
        System.out.println("enter the phone number to  payment");
        String phoneNo2 = null;
        double balance = 0;
        int cusId = 0;

        try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); PreparedStatement pst1 = connection.prepareStatement(insertQuery);) {
            while (true) {
                phoneNo2 = br.readLine();
                pst.setString(1, phoneNo2);
                ResultSet r = pst.executeQuery();
                r.absolute(1);
                balance = r.getDouble(1);
                cusId = r.getInt(2);
                System.out.println("your balance:" + balance);
                break;
            }
            
            System.out.println(pm.getPaymentId() + "payment id");
            LocalDate date = LocalDate.now();
            pst1.setInt(1, pm.getPaymentId());
            pst1.setInt(2, pm.getCustomerId());
            pst1.setDouble(3, pm.getPayAmount());
            pst1.setDate(4, Date.valueOf(date));

            if (pm.getPayAmount() < balance) {
                String status = "not completed";
                double updateTotal = balance - pay;
                System.out.println(updateTotal + "balance - pay");
                ucb.updatePaymentBalance(connection, cusId, updateTotal, status);

            } else if (pm.getPayAmount() == balance) {
                String status = "completed";
                ucb.updatePaymentBalance(connection, cusId, 0, status);

            } else if (pm.getPayAmount() > balance) {

                System.out.println("enter so much value");
            } else if (balance == 0) {
                System.out.println("no balance in due");

            }
            pst1.execute();

        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void viewPaymentDetail() {
         MysqlDatabaseConnection database = new MysqlDatabaseConnection();
           ResultSet r;
        String query = "select name,phone,adress,balance,status,balancestatus from cust where status='active'";
        try (Connection connection=database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query);) {
            r = pst.executeQuery();
            System.err.println("    *************payment Detail************   ");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            System.out.format("\n|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance", "status", "balanceStatus");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            while (r.next()) {
                Payment p=new Payment(r.getInt(1),r.getInt(2),LocalDate.parse(r.getString(3)),r.getDouble(4));
                System.out.println(p);
                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
