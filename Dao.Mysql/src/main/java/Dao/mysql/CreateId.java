package Dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateId {

    ResultSet r;
    CheckOrderTable c = new CheckOrderTable();

    public int generateOrderId() {
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        try (Connection connection = database.CreateConnection();PreparedStatement pst3 = connection.prepareStatement("select * from katabookorder", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {
            if (!c.Isempty(connection)) {
                r = pst3.executeQuery();
                r.last();
                return r.getInt(1) + 1;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 1;
    }

    public int generatepaymentId() {
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        try (Connection connection = database.CreateConnection();PreparedStatement pst3 = connection.prepareStatement("select * from payment", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {
            if (!c.PaymentIsempty(connection)) {
                r = pst3.executeQuery();
                r.last();
                return r.getInt(1) + 1;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 1;
    }
    public int getCustomerId(){
         MysqlDatabaseConnection database = new MysqlDatabaseConnection();
          try (Connection connection = database.CreateConnection();PreparedStatement pst3 = connection.prepareStatement("select * from cust where");) {
            if (!c.PaymentIsempty(connection)) {
                r = pst3.executeQuery();
                r.last();
                return r.getInt(1) + 1;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    
        return 0;
    
    
    }
}
