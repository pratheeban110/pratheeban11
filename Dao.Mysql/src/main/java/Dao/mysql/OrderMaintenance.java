/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Model.LineItems;
import Model.Orders;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200175
 */
public class OrderMaintenance {

    public Orders getOrderObject() {
        DaoOrdersImplementation doi=new DaoOrdersImplementation();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        CreateId c = new CreateId();
        int cusId = doi.getCustId();
        System.out.println(cusId + "cust id");
        int orderId = c.generateOrderId();
        System.out.println(cusId + "order id");
        LocalDate date = LocalDate.now();
        double total = 0;
        double profit = 0;
        System.out.println(total + "order total");
        return new Orders(orderId, cusId, date, total, profit);

    }

    public AddLineitemsObject getLineItemObject(Connection con, int order_id, int cusId) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        UpdateCustomerBalance upb = new UpdateCustomerBalance();
        ResultSet r;
        String query = "Select * from inventory where productid=?";
        double sum = 0;
        double profitSum = 0;
        LineItems[] l = new LineItems[0];
        AddLineitemsObject al = null;

        boolean add = true;

        boolean productavail = false;
        boolean quantityavail = false;
        CreateOrderMenu m = new CreateOrderMenu();
        m.OrderMenu(con);
        while (add) {
            try (PreparedStatement pst1 = con.prepareStatement("insert into katabooklineitems values(?,?,?,?,?)"); PreparedStatement pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {

                l = Arrays.copyOf(l, l.length + 1);
                int orderId = order_id;
                System.out.println(orderId + "line orderid");
                int productId = 0;
                int quantity = 0;
                do {

                    productavail = false;
                    quantityavail = false;

                    System.out.println("enter product id");
                    while (true) {
                        try {
                            productId = Integer.parseInt(br.readLine());
                            pst.setInt(1, productId);
                            r = pst.executeQuery();
                            break;
                        } catch (IOException ex) {
                            System.out.println("IOException occurs");
                            System.out.println("enter detail");
                        } catch (NumberFormatException nfe) {
                            System.out.println("NumberformatException");
                            System.out.println("enter the correct detai");
                        }
                    }

                    if (r.next()) {
                        productavail = true;
                        System.out.println("enter the quantity");
                        while (true) {
                            try {
                                quantity = Integer.parseInt(br.readLine());
                                break;
                            } catch (IOException ex) {
                                System.out.println("IOException occurs");
                                System.out.println("enter detail");
                            } catch (NumberFormatException nfe) {
                                System.out.println("NumberformatException");
                                System.out.println("enter the correct detai");
                            }
                        }
                        int getQuantity = r.getInt(3);
                        if (quantity <= getQuantity) {
                            quantityavail = true;
                        }
                        if (quantityavail == false) {
                            System.out.println("quantity not available");
                            System.out.println("plese enter available quantity");
                        } else {
                            System.out.println("available");
                        }
                    }

                } while (!productavail || !quantityavail);

                double actualPrice = r.getDouble(4);
                double sellingPrice = r.getDouble(5);
                double total = sellingPrice * quantity;
                sum += total;
                double profit = total - (actualPrice * quantity);
                profitSum += profit;
                int qn = r.getInt(3) - quantity;
                System.out.println(qn);
                l[l.length - 1] = new LineItems(orderId, productId, quantity, total, profit);
                System.out.println(total + "i item total");
                upb.updateBalance(con, cusId, total);
                r.absolute(1);
                r.updateInt(3, qn);
                r.updateRow();
                //add multiple line items      
                System.out.println("1.add item again");
                System.out.println("2.enough");
                System.out.println("enter the option");
                int op = 0;
                while (true) {
                    try {
                        op = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOException occurs");
                        System.out.println("enter the detail again");
                    } catch (NumberFormatException nfe) {
                        System.out.println("NumberformatException");
                        System.out.println("enter the correct detai");
                    }
                }
                switch (op) {
                    case 1:
                        add = true;
                        break;
                    case 2:
                        add = false;
                        al = new AddLineitemsObject(l, sum, profitSum);
                        break;
                    default:
                        System.err.println("enter the correct option");

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

        return al;
    }

}
