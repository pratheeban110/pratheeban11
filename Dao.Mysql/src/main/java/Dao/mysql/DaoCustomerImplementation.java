package Dao.mysql;

import Dao.BussinesslLogic.DaoCustomer;
import Model.Customer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoCustomerImplementation implements DaoCustomer {

    @Override
    public void addCustomer(Customer c) {
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        String customerInsert = "insert into  cust values(?,?,?,?,?,?,?)";
        try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareCall(customerInsert);) {
            pst.setInt(1, c.getCusId());
            pst.setString(2, c.getName());
            pst.setString(3, c.getPhoneNumber());
            pst.setString(4, c.getAddress());
            pst.setDouble(5, c.getBalanceAmount());
            pst.setString(6, c.getStatus());
            pst.setString(7, c.getBalanceStatus());
            pst.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void viewCustomer() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        String query = "select * from cust";
        String query2 = "select * from cust where phone=?";
        ResultSet r;
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;

            } catch (IOException nfe1) {
                System.out.println("enter the correct option exception");
                System.out.println("enter correct detail");
                // nfe1.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            }

        }
        switch (view) {

            case 1 -> {

                while (true) {

                    try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query2);) {
                        System.out.println("enter the phone number");
                        String phoneNo2 = br.readLine();
                        pst.setString(1, phoneNo2);
                        r = pst.executeQuery();
                        System.err.println("    *************Customer Detail************   ");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        while (r.next()) {
                            Customer c = new Customer(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getDouble(5), r.getString(6), r.getString(7));
                            System.out.println(c);
                            System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        }
                        break;
                    } catch (IOException ex) {
                        System.out.println("exception occurs");
                        System.out.println("enter the correct detail");

                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            }

            case 2 -> {
                try (Connection connection = database.CreateConnection(); PreparedStatement pst = connection.prepareStatement(query);) {
                    r = pst.executeQuery();

                    System.err.println("    *************Customer Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    while (r.next()) {
                        Customer c = new Customer(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getDouble(5), r.getString(6), r.getString(7));
                        System.out.println(c);
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");

                    }
                    System.out.println("");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            default ->
                System.out.println("enter the correct option");
        }

    }

    @Override
    public void updateCustomerName(Customer c) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();

        try (Connection connection = database.CreateConnection(); PreparedStatement pst1 = connection.prepareStatement("update cust set address=? where cusId="); PreparedStatement pst2 = connection.prepareStatement("update cust set name=? where cusId=");) {
            pst2.setString(1, c.getName());
            pst2.setInt(2, c.getCusId());
            pst2.execute();
            System.out.println("updated");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void updateCustomerAddress(Customer c) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();

        try (Connection connection = database.CreateConnection(); PreparedStatement pst1 = connection.prepareStatement("update cust set address=? where cusId="); PreparedStatement pst2 = connection.prepareStatement("update cust set name=? where cusId=");) {

            pst1.setString(1, c.getAddress());
            pst1.setInt(2, c.getCusId());
            pst1.execute();
            System.out.println("updated");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void deleteCustomer(Customer c) {
        MysqlDatabaseConnection database = new MysqlDatabaseConnection();

        try (Connection connection = database.CreateConnection(); PreparedStatement pst1 = connection.prepareStatement("update cust set status=? where cusId=");) {

            pst1.setString(1, c.getStatus());
            pst1.setInt(2, c.getCusId());
            pst1.execute();
            System.out.println("updated");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

}
