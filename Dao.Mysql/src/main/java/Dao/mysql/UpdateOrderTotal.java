/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author bas200175
 */
public class UpdateOrderTotal {
    ResultSet r;
    public void updatetotal(Connection con,double total,double profit,int orderId){
            try(PreparedStatement  pst = con.prepareStatement("update katabookorder set total_amount=? where order_id=?");
                    PreparedStatement  pst1 = con.prepareStatement("update katabookorder set profit=? where order_id=?");) {
            pst.setDouble(1, total);
            pst.setInt(2, orderId);
            pst1.setInt(2, orderId);
            pst1.setDouble(1, profit);
            pst.execute();
            pst1.execute();
            System.out.println("update customer balance sucessfully");
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    }
    }
    

