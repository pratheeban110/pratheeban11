package Dao.mysql;

import Model.Payment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class PaymentMaintenance {

    public Payment getPaymentObject() {
        CreateId c = new CreateId();
        int payId = c.generatepaymentId();
        int cusId = c.getCustomerId();
        LocalDate date = LocalDate.now();
        double pay = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("enter the pay amount");
            pay = Double.parseDouble(br.readLine());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new Payment(payId,cusId,date,pay);
    }

}
