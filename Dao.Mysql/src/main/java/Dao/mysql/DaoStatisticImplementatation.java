/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao.mysql;

import Dao.BussinesslLogic.DaoStatistics;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author bas200175
 */
public class DaoStatisticImplementatation implements DaoStatistics {

    @Override
    public void mostSellingProduct() {
         MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        ResultSet r;
        String query = "select name,phone,adress,balance from cust where balance=(select max(balance) from cust)";
        try (Connection connection=database.CreateConnection();PreparedStatement pst = connection.prepareStatement(query);) {
            r = pst.executeQuery();
            System.err.println("    *************payment Detail************   ");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15));
            System.out.format("\n|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15));
            while (r.next()) {
                System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4));
                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void maximumBalanceDue() {
         MysqlDatabaseConnection database = new MysqlDatabaseConnection();
        ResultSet r;
        String query = "select name,phone,adress,balance from cust where balance=(select min(balance) from cust)";
        try (Connection connection=database.CreateConnection();PreparedStatement pst = connection.prepareStatement(query);) {
            r = pst.executeQuery();
            System.err.println("    *************payment Detail************   ");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            System.out.format("\n|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance");
            System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            while (r.next()) {
                System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4));
                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void completedBalance() {
        
    }

    @Override
    public void mostProfitableProduct() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mostbuyableCustomer() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void orderStatics() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
