package Dao.BussinesslLogic;

import Model.Orders;


public interface DaoOrders {
    public void addOrders(Orders orders);
    public void addLineItems(int orderId,int cusId);
    public void viewOrders();
    public int getCustId();
    
}
