
package Dao.BussinesslLogic;

import Dao.mysql.*;
import Model.Customer;

public interface DaoCustomer {
    public void addCustomer(Customer c);
    public void viewCustomer();
    public void updateCustomerName(Customer c);
    public void updateCustomerAddress(Customer c);
    public void deleteCustomer(Customer c); 
    
}
