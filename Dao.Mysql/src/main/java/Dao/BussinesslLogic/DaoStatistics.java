package Dao.BussinesslLogic;

public interface DaoStatistics {
    public void mostSellingProduct();
    public void maximumBalanceDue();
    public void completedBalance();
    public void mostProfitableProduct();
    public void mostbuyableCustomer();
    public void orderStatics();
    
}
