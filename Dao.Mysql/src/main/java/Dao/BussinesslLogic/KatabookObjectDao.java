/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Dao.BussinesslLogic;

import Dao.mysql.*;

/**
 *
 * @author bas200175
 */
public interface KatabookObjectDao {

    public DaoCustomer getDaoCustomerObject();

    public DaoOrders getDaoOrdersObject();

    public DaoPayment getDaoPaymentObject();

    public DaoStatistics getDaoStatistics();

    public CustomerService getCustomerServiceObject();

    public OrderMaintenance getOrderMaintenanceObject();

    public ProductMaintenance getProductMaintenanceObject();
    
     public PaymentMaintenance getPaymentMaintenanceObject();

}
