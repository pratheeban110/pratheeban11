package Model;

import java.time.LocalDate;

public class Payment {
    private int paymentId;
    private int customerId;
    private LocalDate date;
    private double payAmount;
   

    public Payment() {
       
    }

    public Payment(int paymentId, int customerId, LocalDate date,double payAmount) {
        this.paymentId = paymentId;
        this.customerId = customerId;
        this.date = date;
        this.payAmount = payAmount;
        
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(double payAmount) {
        this.payAmount = payAmount;
    }


    
   
   
    @Override
    public String toString() {
    
        return String.format("\n|%-4d|%-4d|%-15s|%-15.2f|", this.paymentId,this.customerId, 
                this.date,this.payAmount);
    
    }
    
    
}
