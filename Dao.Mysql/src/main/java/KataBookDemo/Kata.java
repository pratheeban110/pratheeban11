package KataBookDemo;

import Dao.BussinesslLogic.*;
import Dao.mysql.*;
import Model.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Kata {

    public static void main(String[] args) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        KatabookObjectDao object = new KatabookObjectImplementation();
        boolean condition = true;
        do {
            System.out.println("1.Customer");
            System.out.println("2.Inventory");
            System.out.println("3.statistics");
            System.out.println("4.Exit");
            System.out.println("enter the option");
            int option = 0;
            while (true) {
                try {

                    option = Integer.parseInt(br.readLine());

                    break;

                } catch (NumberFormatException nfe1) {
                    System.out.println("enter the correct option exception");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }

            switch (option) {
                case 1:
                    boolean condition1 = true;
                    do {
                        System.out.println("1.Add Customer");
                        System.out.println("2.Update Customer");
                        System.out.println("3.View customer detail");
                        System.out.println("4.Order placed");
                        System.out.println("5.add orders");
                        System.out.println("6.payment");
                        System.out.println("7.delete customer");
                        System.out.println("8.back Exist");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option exception");
                            } catch (IOException ex) {
                                System.out.println("Io Exception");
                            }

                        }

                        switch (option) {

                            case 1: {
                               // Customer c = object.getCustomerServiceObject().getCustomerObject();
                                object.getDaoCustomerObject().addCustomer( object.getCustomerServiceObject().getCustomerObject());
                                break;
                            }
                            case 2: {
                                try {
                                    System.out.println("1.change name");
                                    System.out.println("2.change address");
                                    System.out.println("enter the option");
                                    int option1 = Integer.parseInt(br.readLine());
                                    switch (option1) {
                                        case 1 -> {
                                            // Customer c = object.getCustomerServiceObject().updateCustomerName();
                                            object.getDaoCustomerObject().updateCustomerName(object.getCustomerServiceObject().updateCustomerName());
                                            break;
                                        }

                                        case 2 -> {
                                            // Customer c =object.getCustomerServiceObject().updateCustomerAddress();
                                            object.getDaoCustomerObject().updateCustomerAddress(object.getCustomerServiceObject().updateCustomerAddress());
                                            break;
                                        }
                                        default ->
                                            System.err.println("enter the correct option");

                                    }
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }

                            }

                            case 3:
                                object.getDaoCustomerObject().viewCustomer();
                                break;

                            case 4:
                                object.getDaoOrdersObject().viewOrders();
                                break;
                            case 5:
                                
                                object.getDaoOrdersObject().addOrders(object.getOrderMaintenanceObject().getOrderObject());
                                break;
                            case 6:
                                object.getDaoPaymentObject().addPayment(object.getPaymentMaintenanceObject().getPaymentObject(option, option));
                                break;
                            case 7:
                                condition1 = false;
                                break;
                            case 8
                                    
                            default:
                                System.err.println("enter the correct option");
                                break;
                        }

                    } while (condition1);
                    break;
                case 2:
                    boolean condition2 = true;
                    do {
                        System.out.println("1.Add product");
                        System.out.println("2.Update product");
                        System.out.println("3.View product");
                        System.out.println("4.back Exist");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option io exception occurs");
                            } catch (IOException ex) {
                                System.out.println("io exception occurs");
                            }

                        }

                        switch (option) {
                            case 1:
                                book.addProduct(co);
                                break;
                            case 2:
                                book.updateProduct(co);
                                break;

                            case 3:
                                book.Viewproduct(co);
                                break;
                            case 4:
                                condition2 = false;
                                break;
                            default:
                                System.out.println("enter the correct option");
                                break;
                        }

                    } while (condition2);
                    break;
                case 3:
                    boolean condition3 = true;
                    do {
                        System.out.println("1.Maximum balance");
                        System.out.println("2.min balance");
                        System.out.println("3.payment fully completed");
                        System.out.println("4.payment due customer");
                        System.out.println("5.payment detail");
                        System.out.println("6.exit");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option exception");
                            } catch (IOException ex) {
                                System.out.println("io Exception");
                            }

                        }
                        switch (option) {
                            case 1:
                                book.maxbalanceDetail(co);
                                break;
                            case 2:
                                book.minbalanceDetail(co);
                                break;

                            case 3:
                                book.paymentCompleted(co);
                                break;

                            case 4:
                                book.paymentNotCompleted(co);
                                break;
                            case 5:

                                book.paymentDetail(co);
                                break;
                            case 6:
                                condition3 = false;
                                break;
                            default:
                                System.out.println("enter the correct option");
                                break;
                        }

                    } while (condition3);
                    break;
                case 4:
                    condition = false;
                    break;
                default:
                    System.out.println("enter the correct option");
                    break;
            }

        } while (condition);

    }

}
