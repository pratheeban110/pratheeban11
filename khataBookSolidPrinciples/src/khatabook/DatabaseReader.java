/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabook;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200175
 */
public class DatabaseReader {
    
     DataInputStream dis = null;
     
     public void customerDataReader()  {
        
        try {
            dis = new DataInputStream(new FileInputStream("/home/bas200175/Documents/khaatabook/Customer"));
            while (true) {
                int CId = dis.readInt();
                String cname = dis.readUTF();
                String cphno = dis.readUTF();
                String address = dis.readUTF();
                double balance=dis.readDouble();
                KhataBook.book.c = Arrays.copyOf(KhataBook.book.c, KhataBook.book.c.length + 1);
                KhataBook.book.c[KhataBook.book.c.length - 1] = new Customer(CId, cname, cphno, address,balance);
            }
           
        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
            
        } catch (IOException ex) {
            System.out.println("file readed successfully");
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");

               }
            }
    }
     
     public void inventoryDataReader(){
     try {
            dis = new DataInputStream(new FileInputStream("/home/bas200175/Documents/khaatabook/Product"));
            while (true) {
                int pId = dis.readInt();
                String pname = dis.readUTF();
                double price = dis.readDouble();
                double sprice = dis.readDouble();
                int qnty=dis.readInt();
                KhataBook.book.p = Arrays.copyOf(KhataBook.book.p, KhataBook.book.p.length + 1);
                KhataBook.book.p[KhataBook.book.p.length - 1] = new Product(pId, pname, price, sprice,qnty);
              
            }

        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
             System.out.println("file readed sucessfully"); 
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
              
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");

               }
            }
     
     }
     
     public void lineItemDataReader(){
     try {
            dis = new DataInputStream(new FileInputStream("/home/bas200175/Documents/khaatabook/LineItems"));
            while (true) {
                int oId = dis.readInt();
                int pId= dis.readInt();
                int qnty=dis.readInt();
                double price= dis.readDouble();
                double profit=dis.readDouble();
                
                KhataBook.book.l = Arrays.copyOf(KhataBook.book.l, KhataBook.book.l.length + 1);
                KhataBook.book.l[KhataBook.book.l.length - 1] = new LineItems(oId,pId,qnty,price,profit);
              
            }

        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("file readed successfully");
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
                
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");

               }
            }
     
     }
    public void orderDataReader(){
     try {
            dis = new DataInputStream(new FileInputStream("/home/bas200175/Documents/khaatabook/orders"));
            while (true) {
                int oId = dis.readInt();
                int cId= dis.readInt();
                LocalDate date=LocalDate.parse(dis.readUTF());
                double orderamount= dis.readDouble();
                double profit=dis.readDouble();
                KhataBook.book.o = Arrays.copyOf(KhataBook.book.o, KhataBook.book.o.length + 1);
                KhataBook.book.o[KhataBook.book.o.length - 1] = new Orders(oId,cId,date,orderamount,profit);
              
            }

        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
             System.out.println("file readed sucessfully");
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
               
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");

               }
            }
     
     }
    public void paymentDataReader()  {
        
        try {
            dis = new DataInputStream(new FileInputStream("/home/bas200175/Documents/khaatabook/Payments"));
            while (true) {
                int paymentId = dis.readInt();
                int cusId = dis.readInt();
                LocalDate date=LocalDate.parse(dis.readUTF());
                double totalAmount=dis.readDouble();
                double totalpayable=dis.readDouble();
                double payAmount=dis.readDouble();
                double balanceAmount=dis.readDouble();
                KhataBook.book.pa = Arrays.copyOf(KhataBook.book.pa, KhataBook.book.pa.length + 1);
                KhataBook.book.pa[KhataBook.book.pa.length - 1] = new Payment(paymentId,cusId,date,totalAmount,totalpayable,payAmount,balanceAmount);
            }

        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("file readed sucessfully");
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");

               }
            }
    }
}
