/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabook;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author bas200175
 */
public class DataBaseWriter {

    DataOutputStream dos = null;

    public void customerDataWriter() {

        try {
            dos = new DataOutputStream(new FileOutputStream("/home/bas200175/Documents/khaatabook/Customer"));
            for (int i = 0; i < KhataBook.book.c.length; i++) {
               
                    dos.writeInt(KhataBook.book.c[i].getCusId());
                    dos.writeUTF(KhataBook.book.c[i].getName());
                    dos.writeUTF(KhataBook.book.c[i].getPhoneNumber());
                    dos.writeUTF(KhataBook.book.c[i].getAddress());
                    dos.writeDouble(KhataBook.book.c[i].getBalanceAmount());
                    
                }
            

        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");

        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
        } finally {
            try {
                if (Objects.nonNull(dos)) {
                    dos.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }

    }

    public void inventoryDataWriter() {

        try {
            dos = new DataOutputStream(new FileOutputStream("/home/bas200175/Documents/khaatabook/Product"));
            for (int i = 0; i < KhataBook.book.p.length; i++) {
                
                    
                    dos.writeInt(KhataBook.book.p[i].getProducId());
                    dos.writeUTF(KhataBook.book.p[i].getProductName());
                    dos.writeDouble(KhataBook.book.p[i].getProductPrice());
                    dos.writeDouble(KhataBook.book.p[i].getProductSellingPrice());
                    dos.writeInt(KhataBook.book.p[i].getProductQuantity());
                    
                }
            
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");

        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            //System.out.println("null pointer exception");
            npe.printStackTrace();
        } finally {
            try {
                if (Objects.nonNull(dos)) {
                    dos.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }

    }

    public void lineItemsDataWriter() {

        try {
            dos = new DataOutputStream(new FileOutputStream("/home/bas200175/Documents/khaatabook/LineItems"));
            for (int i = 0; i < KhataBook.book.l.length; i++) {
                
                    
                    dos.writeInt(KhataBook.book.l[i].getOrderId());
                    dos.writeInt(KhataBook.book.l[i].getProductId());
                    dos.writeInt(KhataBook.book.l[i].getQuantity());
                    dos.writeDouble(KhataBook.book.l[i].getPrice());
                    dos.writeDouble(KhataBook.book.l[i].getProfit());
                    
                }
            
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");

        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
            npe.printStackTrace();
        } finally {
            try {
                if (Objects.nonNull(dos)) {
                    dos.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }

    }

    public void orderDataWriter() {

        try {
            dos = new DataOutputStream(new FileOutputStream("/home/bas200175/Documents/khaatabook/orders", true));
            for (int i = 0; i < KhataBook.book.o.length; i++) {
                
                    
                    dos.writeInt(KhataBook.book.o[i].getOrderId());
                    dos.writeInt(KhataBook.book.o[i].getCusId());
                    dos.writeUTF(KhataBook.book.o[i].getDate() + "");
                    dos.writeDouble(KhataBook.book.o[i].getOrderAmount());
                    dos.writeDouble(KhataBook.book.o[i].getProfit());
                    
                }
            
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");

        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
            npe.printStackTrace();
        } finally {
            try {
                if (Objects.nonNull(dos)) {
                    dos.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }
    }

    public void paymentDataWriter() {

        try {

            dos = new DataOutputStream(new FileOutputStream("/home/bas200175/Documents/khaatabook/Payments"));
            for (int i = 0; i < KhataBook.book.pa.length; i++) {
                
                   
                    dos.writeInt(KhataBook.book.pa[i].getPaymentId());
                    dos.writeInt(KhataBook.book.pa[i].getCustomerId());
                    dos.writeUTF(KhataBook.book.pa[i].getDate() + "");
                    dos.writeDouble(KhataBook.book.pa[i].getTotalOderAmount());
                    dos.writeDouble(KhataBook.book.pa[i].getTotalPayable());
                    dos.writeDouble(KhataBook.book.pa[i].getPayAmount());
                    dos.writeDouble(KhataBook.book.pa[i].getBalanceAmount());
                    
                }
            
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");

        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
            npe.printStackTrace();
        } finally {
            try {
                if (Objects.nonNull(dos)) {
                    dos.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }

    }
   
}
