package khatabook;

import java.time.LocalDate;

public class Payment {
    private int paymentId;
    private int customerId;
    private LocalDate date;
    private double totalOderAmount;
    private double totalPayable;
    private double payAmount;
    private double balanceAmount;

    public Payment() {
    }

    public Payment(int paymentId, int customerId, LocalDate date,double totalOrderAmount,double totalPayable,double payAmount,double balanceAmount) {
        this.paymentId = paymentId;
        this.customerId = customerId;
        this.date = date;
        this.totalOderAmount=totalOrderAmount;
        this.totalPayable=totalPayable;
        this.payAmount = payAmount;
        this.balanceAmount=balanceAmount;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(double payAmount) {
        this.payAmount = payAmount;
    }

    public double getTotalPayable() {
        return totalPayable;
    }

    public void setTotalPayable(double totalPayable) {
        this.totalPayable = totalPayable;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public double getTotalOderAmount() {
        return totalOderAmount;
    }

    public void setTotalOderAmount(double totalOderAmount) {
        this.totalOderAmount = totalOderAmount;
    }
   
   
    @Override
    public String toString() {
        return ":Payment Id :  "  + paymentId+"\n" + "Order ID  :  " + customerId 
    + "\n" +"Date  : "+ date+"\n"+"Total Amount:"+totalOderAmount+"\n"+"amount Payable:"
                +totalPayable+"\n"+"Balance payment:"+balanceAmount;
    }
    
    
}
