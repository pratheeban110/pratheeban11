package khatabook;

import java.util.Scanner;

public class Customer {

    private int CusId;
    private String name;
    private String phoneNumber;
    private String address;
    private double balanceAmount;

    Scanner in = new Scanner(System.in);

    public Customer(int Cusid, String name, String phoneNumber, String address, double balance) {
        setCusId(Cusid);
        setName(name);
        setPhoneNumber(phoneNumber);
        setAddress(address);
        setBalanceAmount(balance);
    }

    public Customer() {
        super();
    }

    public void setCusId(int CusId) {
        this.CusId = CusId;
    }

    public void setName(String name) {
        if ((name.length() > 2) && (name.chars().filter(Character::isAlphabetic).count() == name.length())) {
            this.name = name;
        } else {
            System.err.println(" Enter a valid Name");
            setName(in.next());
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        if ((phoneNumber.length()==10)&&(phoneNumber.chars().filter(Character::isDigit).count() == phoneNumber.length())) {
            this.phoneNumber = phoneNumber;
        } else {
            System.err.println(" Enter a phonenumber");
            setPhoneNumber(in.next());
        }
    }

    public int getCusId() {
        return CusId;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
    

    @Override
    public String toString() {
        return "customerID : " + CusId + "  "+ "Name: " + name + " " 
                + "phonemumber: " + phoneNumber + "  " + "Address: " 
                + address+"  "+"BalanceAmount: "+balanceAmount;
    }

}
