package khatabook;

public class LineItems {

    
    private int orderId;
    private int productId;
    private int quantity;
    private double price;
    private double profit;
  

    public LineItems() {
        super();
    }

    public LineItems( int orderId, int productId, int quantity,double price,double profit) {
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quantity;
        this.price=price;
        this.profit=profit;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
    

    @Override
    public String toString() {
        return   "order: " + orderId + "\n" + "product id:  " +
                productId + "\n" + "Quantity :" + quantity+"\n"+"price:"+price;
    }

}
