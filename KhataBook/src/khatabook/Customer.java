package khatabook;

import java.util.Scanner;

public class Customer {

    private int CusId;
    private String name;
    private String phoneNumber;
    private String address;

    Scanner in = new Scanner(System.in);

    public Customer(int Cusid, String name, String phoneNumber, String address) {
        setCusId(CusId);
        setName(name);
        setPhoneNumber(phoneNumber);
        setAddress(address);
    }

    public Customer() {
        super();
    }

    public void setCusId(int CusId) {
        this.CusId = CusId;
    }

    public void setName(String name) {
        if ((name.length() > 2) && (name.chars().filter(Character::isAlphabetic).count() == name.length())) {
            this.name = name;
        } else {
            System.err.println(" Enter a valid Name");
            setName(in.next());
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        if ((phoneNumber.chars().filter(Character::isDigit).count() == phoneNumber.length())) {
            this.phoneNumber = phoneNumber;
        } else {
            System.err.println(" Enter a phonenumber");
            setPhoneNumber(in.next());
        }
    }

    public int getCusId() {
        return CusId;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "cutomer ID:" + CusId + "\n" + "Name:" + name + "\n" + "phonemumber:" + phoneNumber + "\n" + "Address:" + address;
    }

}
