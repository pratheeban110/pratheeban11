package khatabook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

public class KhataBook {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static KhataBook book = new KhataBook();
    int i = 1;
    Customer[] c = new Customer[5];
    Product[] p = new Product[5];
    LineItems[] l = new LineItems[0];
    Orders[] o = new Orders[0];
    Payment[] pa = new Payment[0];

    public void addCustomer() {
        try {
            c = Arrays.copyOf(c, c.length + 1);
            System.out.println("cusid is" + " " + i);
            int cusId = i++;
            System.out.println("enter the name");
            String name = br.readLine();
            System.out.println("enter the phone number");
            String phoneNumber = br.readLine();
            System.out.println("enter the address");
            String address = br.readLine();
            c[c.length - 1] = new Customer(cusId, name, phoneNumber, address);
            System.out.println("customer added sucessfully");
            System.out.println("");

        } catch (IOException ex) {
            System.out.println("exception occurs");
            ex.printStackTrace();
        }

    }

    public void viewCustomer() {
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;

            } catch (IOException nfe1) {
                System.out.println("enter the correct option exception");
                System.out.println("enter correct detail");
                // nfe1.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            }

        }
        switch (view) {
            case 1 -> {
                System.out.println("enter the phone number");
                String phoneNo2 = null;
                while (true) {
                    try {
                        phoneNo2 = br.readLine();
                        break;
                    } catch (IOException ex) {
                        System.out.println("exception occurs");
                        System.out.println("enter the correct detail");
                        //ex.printStackTrace();
                    }
                }
                for (int i = 0; i < c.length; i++) {
                    if (Objects.nonNull(phoneNo2) && c[i].getPhoneNumber().equals(phoneNo2)) {
                        System.err.println("    *************Customer Detail************   ");
                        System.out.println(c[i]);
                        System.out.println("");

                    }
                }
            }

            case 2 -> {
                System.err.println("    *************Customer Detail************   ");
                for (int j = 0; j < c.length; j++) {
                    System.out.println(c[j]);
                    System.out.println("");
                }
            }

            default ->
                System.out.println("enter the correct option");
        }

    }

    public void updateCustomer() {
        while (true) {
            try {
                System.out.println("enter the phone number");
                String phoneNo2 = br.readLine();

                for (int i = 0; i < c.length; i++) {
                    if (c[i].getPhoneNumber().equals(phoneNo2)) {
                        System.out.println("1.name");
                        System.out.println("2.address");
                        System.out.println("");
                        System.out.println("enter the feild to chane");
                        int option = Integer.parseInt(br.readLine());
                        switch (option) {
                            case 1:
                                System.out.println("enter the new name");
                                String name = br.readLine();
                                if (Objects.nonNull(name)) {
                                    c[i].setName(name);
                                }
                                break;
                            case 2:
                                System.out.println("enter the address");
                                String address = br.readLine();
                                if (Objects.nonNull(address)) {
                                    c[i].setAddress(address);
                                }
                            default:
                                System.err.println("enter the correct option");
                                break;

                        }

                    }
                }
            } catch (IOException ex) {
                System.out.println("Io exception occurs");
                System.out.println("enter the correct detail");
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            }
        }
    }

    public void addProduct() {
        while (true) {
            try {
                p = Arrays.copyOf(p, p.length + 1);
                System.out.println("enter the product id");
                int productId = Integer.parseInt(br.readLine());
                System.out.println("enter the product name");
                String productName = br.readLine();
                System.out.println("enter the product price");
                double productPrice = Double.parseDouble(br.readLine());
                System.out.println("enter the product selling price");
                double sellingPrice = Double.parseDouble(br.readLine());
                System.out.println("enter the quantity");
                int productQuantity = Integer.parseInt(br.readLine());
                if (Objects.nonNull(productName)) {
                    p[p.length - 1] = new Product(productId, productName, productPrice, sellingPrice, productQuantity);
                }
                System.out.println("product added sucessfully");
                break;
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
                System.out.println("re enter correct detail");
            } catch (NumberFormatException nfe) {
                System.out.println("Number format exception");
                System.out.println("re enter correct detail");
            }

        }
    }

    public void Viewproduct() {
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;
            } catch (IOException ex) {
                System.out.println("io exception occurs");
            } catch (NumberFormatException nfe) {
                System.out.println("Number format ecxeption occur enter correct detail");
            }
        }
        switch (view) {
            case 1 -> {
                System.out.println("enter the product Id");
                int id = 0;
                while (true) {
                    try {
                        id = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOexception occurs");
                    } catch (NumberFormatException nfe) {
                        System.out.println("number format exception occurs enter correct detail");
                    }
                }
                for (int j = 0; j < p.length; j++) {
                    if (p[j].getProducId() == id) {
                        System.err.println("    *************Product Detail************   ");
                        System.out.println(p[j]);
                    }

                }
            }

            case 2 -> {
                System.err.println("    *************Product Detail************   ");
                for (int j = 0; j < p.length; j++) {
                    System.out.println(p[j]);
                    System.out.println("");
                }
            }
            default ->
                System.err.println("enter the correct option");
        }

    }

    public void updateProduct() {
        while (true) {
            try {
                System.out.println("enter the product id");
                int id = Integer.parseInt(br.readLine());
                for (int i = 0; i < p.length; i++) {
                    if (p[i].getProducId() == id) {
//                System.out.println("1.product quantity");
//                System.out.println("");
                        System.out.println("enter the quantity to chane");
//                 int option = scn.nextInt();
                        int qnty = Integer.parseInt(br.readLine());
                        p[i].setProductQuantity(qnty);

                    }
                }
            } catch (IOException ex) {
                System.out.println("exception occurs");
                ex.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("Numberformat exception occurs");
                System.out.println("enter correct detail");
            }
        }
    }

    public void placeOrder() {
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;
            } catch (IOException ex) {
                System.out.println("exception occurs place order option");
            } catch (NumberFormatException nfe) {
                System.out.println("Numberformat exception occurs");
                System.out.println("enter correct detail");
            }
        }
        switch (view) {
            case 1 -> {
                System.out.println("enter the phone number");
                String phoneNo2 = null;
                while (true) {
                    try {
                        phoneNo2 = br.readLine();
                        break;
                    } catch (IOException ex) {
                        System.out.println("exception occurs place order getting phone number");
                    }
                }
                for (int i = 0; i < c.length; i++) {
                    if (Objects.nonNull(phoneNo2)) {
                        if (c[i].getPhoneNumber().equals(phoneNo2)) {

                            int cusid = c[i].getCusId();
                            for (int j = 0; j < o.length; j++) {

                                if (o[j].getCusId() == cusid) {

                                    System.out.println("    *************place order Detail************   ");
                                    System.out.println(o[j]);
                                    System.out.println("");
                                    for (int k = 0; k < l.length; k++) {
                                        if (o[j].getOrderId() == l[k].getOrderId()) {
                                            System.out.println(l[k]);
                                        }
                                        System.out.println(" ");

                                    }
                                }

                            }
                        }
                    }
                }
            }

            case 2 -> {
                for (int j = 0; j < o.length; j++) {
                    System.out.println("    *************place order Detail************   ");
                    System.out.println(o[j]);
                    System.out.println("");
                    for (int k = 0; k < l.length; k++) {

                        System.out.println(l[k]);
                        System.out.println(" ");

                    }
                }
            }

            default ->
                System.err.println("enter the correct option");
        }
    }

    public void addOrders() {

        System.out.println("enter the phone number to add orders");
        String phoneNo2 = null;
        while (true) {
            try {
                phoneNo2 = br.readLine();
                break;
            } catch (IOException ex) {
                System.out.println("IOException occurs");
                System.out.println("enter detai again");
            }
        }
        for (int i = 0; i < c.length; i++) {
            if (Objects.nonNull(phoneNo2)) {
                if (book.c[i].getPhoneNumber().equals(phoneNo2)) {
                    book.addItems();
                    o = Arrays.copyOf(o, o.length + 1);
                    System.out.println("enter the cusid already given");
                    int cusId = c[i].getCusId();
                    System.out.println("orderid is " + (l[l.length - 1].getOrderId()));
                    int orderId = l[l.length - 1].getOrderId();
                    System.out.println(orderId);
                    LocalDate date = LocalDate.now();
                    double total = 0;
                    double spTotal = 0;
                    for (int j = 0; j < p.length; j++) {
                        for (int k = 0; k < l.length; k++) {
                            if (l[k].getOrderId() == orderId) {
                                if (p[j].getProducId() == l[k].getProductId()) {
                                    spTotal = spTotal + p[j].getProductSellingPrice() * l[k].getQuantity();
                                    total = total + p[j].getProductPrice() * l[k].getQuantity();
                                }

                            }

                        }
                    }
                    double orderTotal = spTotal;
                    double profit = spTotal - total;
                    System.out.println("enter the deliveryAddress");
                    String deliveryAddress = null;
                    try {
                        deliveryAddress = br.readLine();
                    } catch (IOException ex) {
                        System.out.println("IOException occurs");
                        System.out.println("enter the detai");
                    }
                    if (Objects.nonNull(deliveryAddress)) {
                        o[o.length - 1] = new Orders(orderId, cusId, date, orderTotal, spTotal, deliveryAddress, profit);
                    }
                    System.out.println("customer added sucessfully");
                    System.out.println("order add sucess fully");

                }
            }
        }
    }
    int orid = 1;

    public void addItems() {
        boolean add = true;

        boolean productavail = false;
        boolean quantityavail = false;

        for (int j = 0; j < p.length; j++) {
            System.out.println(p[j]);
            System.out.println(" ");
        }
        int orderId = orid++;
        System.out.println(orderId);

        while (add) {
            l = Arrays.copyOf(l, l.length + 1);

            int productId = 0;
            int quantity = 0;
            do {
                productavail = false;
                quantityavail = false;

                System.out.println("enter product id");
                while (true) {
                    try {
                        productId = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOException occurs");
                        System.out.println("enter detail");
                    } catch (NumberFormatException nfe) {
                        System.out.println("NumberformatException");
                        System.out.println("enter the correct detai");
                    }
                }

                for (int j = 0; j < p.length; j++) {
                    if (p[j].getProducId() == productId) {
                        productavail = true;
                        System.out.println("enter the quantity");
                        while (true) {
                            try {
                                quantity = Integer.parseInt(br.readLine());
                                break;
                            } catch (IOException ex) {
                                System.out.println("IOException occurs");
                                System.out.println("enter detail");
                            } catch (NumberFormatException nfe) {
                                System.out.println("NumberformatException");
                                System.out.println("enter the correct detai");
                            }
                        }
                        if (quantity <= p[j].getProductQuantity()) {
                            quantityavail = true;
                        }
                        if (quantityavail == false) {
                            System.out.println("quantity not available");
                            System.out.println("plese enter available quantity");
                        } else {
                            System.out.println("available");
                        }
                    }

                }

            } while (!productavail || !quantityavail);

            double actualPrice = 0;
            double sellingPrice = 0;
            for (int j = 0; j < p.length; j++) {
                if (p[j].getProducId() == productId) {
                    actualPrice = actualPrice + p[j].getProductPrice() * quantity;
                    sellingPrice = sellingPrice + p[j].getProductSellingPrice() * quantity;
                    int t = p[j].getProductQuantity() - quantity;
                    p[j].setProductQuantity(t);
                }

            }
            double profit = sellingPrice - actualPrice;
            l[l.length - 1] = new LineItems(orderId, productId, quantity, sellingPrice, profit);
            System.out.println("1.add item again");
            System.out.println("2.enough");
            System.out.println("enter the option");
            int op = 0;
            while (true) {
                try {
                    op = Integer.parseInt(br.readLine());
                    break;
                } catch (IOException ex) {
                    System.out.println("IOException occurs");
                    System.out.println("enter the detail again");
                } catch (NumberFormatException nfe) {
                    System.out.println("NumberformatException");
                    System.out.println("enter the correct detai");
                }
            }
            switch (op) {
                case 1 ->
                    add = true;
                case 2 ->
                    add = false;
                default ->
                    System.err.println("enter the correct option");

            }

        }
    }

    public void payment() {
        System.out.println("enter the phone number to add orders");
        String phoneNo2 = null;
        while (true) {
            try {
                phoneNo2 = br.readLine();
                break;
            } catch (IOException ex) {
                System.out.println("IOException occurs");
                System.out.println("enter the detail");
            }
        }
        for (int i = 0; i < c.length; i++) {
            if (Objects.nonNull(phoneNo2) && book.c[i].getPhoneNumber().equals(phoneNo2)) {
                int cusId = c[i].getCusId();
                for (int j = 0; j < o.length; j++) {
                    if (o[j].getCusId() == cusId) {
                        int id = pa.length + 1;
                        int orId = o[j].getOrderId();
                        System.out.println("total price " + o[j].getTotalPrice());
                        double orderTotal = o[j].getTotalPrice();
                        double total = o[j].getTotalPrice();
                        LocalDate date = LocalDate.now();
                        System.out.println("Enter the Amount pay by customer");
                        double pay = 0;
                        try {
                            pay = Double.parseDouble(br.readLine());
                        } catch (IOException ex) {
                            System.out.println("io exception occurs");
                        } catch (NumberFormatException nfe) {
                            System.out.println("NumberformatException");
                            System.out.println("enter the correct detai");
                        }
                        double balance = o[j].getTotalPrice() - pay;
                        System.out.println("customer payable amount :" + balance);
                        pa = Arrays.copyOf(pa, pa.length + 1);
                        pa[pa.length - 1] = new Payment(id, orId, date, orderTotal, total, pay, balance);
                        o[j].setTotalPrice(balance);

                    }

                }

            }
        }
    }

    public void paymentDetail() {
        while (true) {
            try {
                String s = null;
                String e = null;
                System.out.println("enter start date");
                s = br.readLine();
                System.out.println("enter end date");
                e = br.readLine();
                LocalDate start = LocalDate.parse(s);
                LocalDate end = LocalDate.parse(e);
                double total = 0;
                double balance = 0;
                while (!start.isAfter(end)) {
                    for (int j = 0; j < o.length; j++) {
                        if (pa[i].getDate().equals(start)) {
                            total = total + pa[i].getPayAmount();
                            balance = balance + pa[i].getBalanceAmount();
                            System.out.println(pa[i]);
                        }
                    }
                    System.out.println("total AmountPaid: " + total);
                    System.out.println("total BalanceAmount: " + balance);
                    start = start.plusDays(1);
                }
                break;
            } catch (IOException ex) {
                System.out.println("IOException");
                System.out.println("enter detail again");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception occurs");
                System.out.println("enter detail again");
            }
        }
    }

    public void orderDetail() {
        while (true) {
            try {
                System.out.println("enter start date");
                String s = br.readLine();
                System.out.println("enter end date");
                String e = br.readLine();

                LocalDate start = LocalDate.parse(s);
                LocalDate end = LocalDate.parse(e);
                while (!start.isAfter(end)) {
                    for (int j = 0; j < o.length; j++) {
                        if (o[i].getDate().equals(start)) {
                            System.out.println(o[i]);
                        }
                    }
                    start = start.plusDays(1);
                    break;
                }
            } catch (IOException ex) {
                System.out.println("IOException occurs");
                System.out.println("enter detail again");
            } catch (NullPointerException npe) {
                System.out.println("NullPointerException occurs");
                System.out.println("enter the correct detail");
            }
        }
    }

    public void mostSellingProduct() {
        int proid = l[0].getProductId();
        for (int i = 1; i < l.length; i++) {
            if (l[i - 1].getQuantity() <= l[i].getQuantity()) {
                proid = l[i].getProductId();
            }
        }
        for (int i = 0; i < p.length; i++) {
            if (p[i].getProducId() == proid) {
                System.out.println(p[i]);
            }
            System.out.println(" ");
        }

    }

    public void mostprofitProduct() {
        int proid = l[0].getProductId();
        for (int i = 1; i < l.length; i++) {
            if (l[i - 1].getProfit() <= l[i].getProfit()) {
                proid = l[i].getProductId();
            }
        }
        for (int i = 0; i < p.length; i++) {
            if (p[i].getProducId() == proid) {
                System.out.println(p[i]);
            }
            System.out.println(" ");
        }

    }

    public static void main(String[] args) throws IOException {

        book.c[0] = new Customer(1, "vijay", "7868985612", "chennai");
        book.c[1] = new Customer(2, "ajith", "7868768674", "trichy");
        book.c[2] = new Customer(3, "arun", "8870141765", "madurai");
        book.c[3] = new Customer(4, "kumar", "7800000121", "coimbatore");
        book.c[4] = new Customer(5, "kannan", "8870141767", "chennai");

        book.p[0] = new Product(121, "5050", 8, 10, 120);
        book.p[1] = new Product(122, "7up", 30, 35, 70);
        book.p[2] = new Product(123, "lays", 8, 10, 121);
        book.p[3] = new Product(124, "bingo", 7, 10, 125);
        book.p[4] = new Product(125, "5050", 8, 10, 120);

        boolean condition = true;
        do {
            System.out.println("1.Customer");
            System.out.println("2.Inventory");
            System.out.println("3.statistics");
            System.out.println("4.Exit");
            System.out.println("enter the option");
            int option = 0;
            while (true) {
                try {
                    option = Integer.parseInt(br.readLine());
                    break;

                } catch (NumberFormatException nfe1) {
                    System.out.println("enter the correct option exception");
                }

            }

            switch (option) {
                case 1:
                    boolean condition1 = true;
                    do {
                        System.out.println("1.Add Customer");
                        System.out.println("2.Update Customer");
                        System.out.println("3.View customer detail");
                        System.out.println("4.Order placed");
                        System.out.println("5.add orders");
                        System.out.println("6.payment");
                        System.out.println("7.back Exist");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option exception");
                            }

                        }

                        switch (option) {
                            case 1:

                                book.addCustomer();
                                break;

                            case 2:
                                book.updateCustomer();
                                break;

                            case 3:
                                book.viewCustomer();
                                break;

                            case 4:
                                book.placeOrder();
                                break;
                            case 5:
                                book.addOrders();
                                break;
                            case 6:
                                book.payment();
                                break;
                            case 7:
                                condition1 = false;
                                break;
                            default:
                                System.err.println("enter the correct option");
                                break;
                        }

                    } while (condition1);
                    break;
                case 2:
                    boolean condition2 = true;
                    do {
                        System.out.println("1.Add product");
                        System.out.println("2.Update product");
                        System.out.println("3.View product");
                        System.out.println("4.back Exist");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option io exception occurs");
                            }

                        }

                        switch (option) {
                            case 1:

                                book.addProduct();
                                break;

                            case 2:
                                book.updateProduct();
                                break;

                            case 3:
                                book.Viewproduct();
                                break;
                            case 4:
                                condition2 = false;
                                break;
                            default:
                                System.out.println("enter the correct option");
                                break;
                        }

                    } while (condition2);
                    break;
                case 3:
                    boolean codition3 = true;
                    do {
                        System.out.println("1.Most selling Proct");
                        System.out.println("2.most profit product");
                        System.out.println("3.order detail");
                        System.out.println("4.payment detai");
                        System.out.println("5.exit");
                        System.out.println("enter the Customer option");
                        while (true) {
                            try {
                                option = Integer.parseInt(br.readLine());
                                break;

                            } catch (NumberFormatException nfe1) {
                                System.out.println("enter the correct option exception");
                            }

                        }
                        switch (option) {
                            case 1:
                                book.mostSellingProduct();
                                break;
                            case 2:
                                book.mostprofitProduct();
                                break;

                            case 3:
                                book.orderDetail();
                                break;

                            case 4:
                                book.paymentDetail();
                                break;
                            case 5:
                                condition2 = false;
                                break;
                            default:
                                System.out.println("enter the correct option");
                                break;
                        }

                    } while (codition3);
                    break;
                case 4:
                    condition = false;
                    break;
                default:
                    System.out.println("enter the correct option");
                    break;
            }
        } while (condition);

    }

}
