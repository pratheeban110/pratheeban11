package khatabook;

import java.time.LocalDate;

public class Payment {
    private int paymentId;
    private int orderId;
    private LocalDate date;
    private double totalOderAmount;
    private double totalPayable;
    private double payAmount;
    private double balanceAmount;

    public Payment() {
    }

    public Payment(int paymentId, int orderId, LocalDate date,double totalOrderAmount,double totalPayable,double payAmount,double balanceAmount) {
        this.paymentId = paymentId;
        this.orderId = orderId;
        this.date = date;
        this.totalOderAmount=totalOrderAmount;
        this.totalPayable=totalPayable;
        this.payAmount = payAmount;
        this.balanceAmount=balanceAmount;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(double payAmount) {
        this.payAmount = payAmount;
    }

    public double getTotalPayable() {
        return totalPayable;
    }

    public void setTotalPayable(double totalPayable) {
        this.totalPayable = totalPayable;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
   
   
    @Override
    public String toString() {
        return ":Payment Id :  "  + paymentId+"\n" + "Order ID  :  " + orderId 
                + "\n" +"Date  : "+ date+"\n"+"Total Amount:"+totalOderAmount+"\n"+"amount Payable:"+totalPayable+"\n"+"Balance payment:"+balanceAmount;
    }
    
    
}
