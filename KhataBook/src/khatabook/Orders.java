 package khatabook;

import java.time.LocalDate;
import java.util.Scanner;

public class Orders {
    private int orderId;
    private int cusId;
    private LocalDate date;
    private double orderAmount;
    private double totalPrice;
    private String deliveredAddress;
    private double profit;

    Orders[] o = new Orders[0];

    public Orders(int orderId, int cusId, LocalDate date,double orderAmount, double totalPrice, String deliveredAddress,double profit) {
        this.orderId = orderId;
        this.cusId = cusId;
        this.date = date;
        this.orderAmount=orderAmount;
        this.totalPrice = totalPrice;
        this.deliveredAddress = deliveredAddress;
        this.profit=profit;
    }

    

    public Orders() {
        super();
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

   

    public void setDeliveredAddress(String deliveredAddress) {
        this.deliveredAddress = deliveredAddress;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getCusId() {
        return cusId;
    }

    

    public String getDeliveredAddress() {
        return deliveredAddress;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }
    
    

    @Override
    public String toString() {
        return  ":orderId :  "  + orderId+"\n" + "cutomer ID  :  " + cusId  + "\n" +"Date  :   "+ date+"\n"+"deliveredAdress : " 
                + deliveredAddress+"\n"+"Total Amount:"+orderAmount+"\n"+"amount Payable:"+totalPrice;
    }

}
