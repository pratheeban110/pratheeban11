/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.mavenproject1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author bas200175
 */
public class Mavenproject1 {

    public static void main(String[] args) {
        String url = "jdbc:mysql://bassure.in:3306/pratheeban11_db8";
        String user = "pratheeban11";
        String pass = "pratheeban@11";
        int age = 23;

        String query1 = "select * from users where age=" + age;
        try (Connection con = DriverManager.getConnection(url, user, pass); Statement s = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            System.out.println("connected");

            ResultSet r = s.executeQuery(query1);

            System.out.println("done");

            while (r.next()) {

                String w = r.getInt("user_id") + ":" + r.getString("e_name") + ":" + r.getInt("age");
                System.out.println(w);
            }
            r.absolute(3);
            r.updateString(4, "dinakaran65@gmail.com");
            r.updateRow();
            System.out.println("update sucessfully");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
