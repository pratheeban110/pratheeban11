/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookDatabaseImplementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bas200175
 */
public class CreateOrderMenu {
    public void OrderMenu(Connection con){
        try {
            String query = "select * from inventory";
            Statement s = con.createStatement();
            ResultSet r;
            r = s.executeQuery(query);
           System.out.print("\n+" + "-".repeat(9) + "+" + "-".repeat(10) + "+" + "-".repeat(11) + "+" + "-".repeat(15) + "+");
            System.out.printf("\n|%-9s|%-10s|%-11s|%-15s|", "ID ", " Name ", " Quantity  ", "product_price ");
            System.out.print("\n+" + "-".repeat(9) + "+" + "-".repeat(10) + "+" + "-".repeat(11) + "+" + "-".repeat(15) + "+");
            while (r.next()) {
                System.out.format("\n|%-9s|%-10s|%-11s|%-15s|", r.getInt(1), r.getString(2), r.getInt(3), r.getFloat(5));
                System.out.print("\n+" + "-".repeat(9) + "+" + "-".repeat(10) + "+" + "-".repeat(11) + "+" + "-".repeat(15) + "+");
                System.out.println("");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateOrderMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    }
}
