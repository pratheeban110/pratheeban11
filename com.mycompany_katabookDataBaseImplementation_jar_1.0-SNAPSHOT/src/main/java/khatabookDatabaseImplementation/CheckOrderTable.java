/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookDatabaseImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author bas200175
 */
public class CheckOrderTable {
    ResultSet r;
    
    public boolean Isempty(Connection con){
     try(PreparedStatement pst3=con.prepareStatement("select * from katabookorder",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);){
       r=pst3.executeQuery();
       return !r.absolute(1);
       
     }  catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    
    }
    
     public boolean PaymentIsempty(Connection con){
     try(PreparedStatement pst3=con.prepareStatement("select * from payment",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);){
       r=pst3.executeQuery();
       return !r.absolute(1);
       
     }  catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    
    }
}
