package khatabookDatabaseImplementation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KhataBook {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static KhataBook book = new KhataBook();
    int intialOrderId = 0;

    public void addCustomer(Connection con) {
        int i = 0;
        String query = "insert into  cust values(?,?,?,?,?)";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            System.out.println("cusid is" + " " + i);
            int cusId = 0;
            pst.setInt(1, cusId);
            System.out.println("enter the name");
            String name = br.readLine();
            pst.setString(2, name);
            System.out.println("enter the phone number");
            String phoneNumber = br.readLine();
            pst.setString(3, phoneNumber);
            System.out.println("enter the address");
            String address = br.readLine();
            pst.setString(4, address);
            double balance = 0;
            pst.setDouble(5, balance);
            pst.execute();
            System.out.println("customer added sucessfully");

        } catch (IOException ex) {
            System.out.println("exception occurs");
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void viewCustomer(Connection con) {
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        String query = "select * from cust";
        String query2 = "select * from cust where phone=?";
        ResultSet r;
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;

            } catch (IOException nfe1) {
                System.out.println("enter the correct option exception");
                System.out.println("enter correct detail");
                // nfe1.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            }

        }
        switch (view) {

            case 1 -> {

                while (true) {

                    try (PreparedStatement pst = con.prepareStatement(query2);) {
                        System.out.println("enter the phone number");
                        String phoneNo2 = br.readLine();
                        pst.setString(1, phoneNo2);
                        r = pst.executeQuery();
                        System.err.println("    *************Customer Detail************   ");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        while (r.next()) {
                            System.out.format("\n|%-4d|%-15s|%-12s|%-25s|%9.2f|", r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getDouble(5));
                            System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                        }
                        break;
                    } catch (IOException ex) {
                        System.out.println("exception occurs");
                        System.out.println("enter the correct detail");

                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            }

            case 2 -> {
                try (PreparedStatement pst = con.prepareStatement(query);) {
                    r = pst.executeQuery();

                    System.err.println("    *************Customer Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-25s|%9s|", "Id", "customer name", "phonenumber", "Address", "Balance");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");
                    while (r.next()) {

                        System.out.format("\n|%-4d|%-15s|%-12s|%-25s|%9.2f|", r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getDouble(5));
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(25) + "+" + "-".repeat(9) + "+");

                    }
                    System.out.println("");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            default ->
                System.out.println("enter the correct option");
        }

    }

    public void updateCustomer(Connection con) {
        ResultSet r;
        String query = "select * from cust where phone=?";
        while (true) {
            try (PreparedStatement pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {
                System.out.println("enter the phone number");
                String phoneNo2 = br.readLine();
                pst.setString(1, phoneNo2);
                r = pst.executeQuery();

                System.out.println("1.name");
                System.out.println("2.address");
                System.out.println("");
                System.out.println("enter the feild to chane");
                int option = Integer.parseInt(br.readLine());
                switch (option) {
                    case 1:

                        System.out.println("enter the new name");
                        String name = br.readLine();
                        r.absolute(1);
                        r.updateString(2, name);
                        r.updateRow();
                        System.out.println("sucusessfully updated......");
                        break;
                    case 2:
                        System.out.println("enter the address");
                        String address = br.readLine();
                        r.absolute(1);
                        r.updateString(4, address);
                        r.updateRow();
                        System.out.println("sucessfully updated......");
                        break;
                    default:
                        System.err.println("enter the correct option");
                        break;

                }

            } catch (IOException ex) {
                System.out.println("Io exception occurs");
                System.out.println("enter the correct detail");
            } catch (NumberFormatException nfe) {
                System.out.println("number format exception occurs");
                System.out.println("enter correct detail");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void addProduct(Connection con) {
        String query = "insert into inventory values(?,?,?,?,?)";
        while (true) {
            try (PreparedStatement pst = con.prepareCall(query)) {
                int productId = 0;
                pst.setInt(1, productId);
                System.out.println("productid :" + productId);
                System.out.println("enter the product name");
                String productName = br.readLine();
                pst.setString(2, productName);
                System.out.println("enter the product price");
                float productPrice = Float.parseFloat(br.readLine());
                pst.setFloat(4, productPrice);
                System.out.println("enter the product selling price");
                float sellingPrice = Float.parseFloat(br.readLine());
                pst.setFloat(5, sellingPrice);
                System.out.println("enter the quantity");
                int productQuantity = Integer.parseInt(br.readLine());
                pst.setInt(3, productQuantity);
                pst.execute();
                System.out.println("product added sucessfully");
                break;
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
                System.out.println("re enter correct detail");
            } catch (NumberFormatException nfe) {
                System.out.println("Number format exception");
                System.out.println("re enter correct detail");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
    }

    public void Viewproduct(Connection con) {
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        ResultSet r;
        String query = "select * from inventory where productid=?";
        String query1 = "select * from inventory";
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;
            } catch (IOException ex) {
                System.out.println("io exception occurs");
            } catch (NumberFormatException nfe) {
                System.out.println("Number format ecxeption occur enter correct detail");
            }
        }
        switch (view) {
            case 1 -> {

                System.out.println("enter the product Id");
                int id = 0;
                while (true) {
                    try {

                        id = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOexception occurs");
                    } catch (NumberFormatException nfe) {
                        System.out.println("number format exception occurs enter correct detail");
                    }
                }
                try (PreparedStatement pst = con.prepareCall(query);) {
                    pst.setInt(1, id);

                    r = pst.executeQuery();

                    System.err.println("    *************product Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|", "Id", "product name", "quantity", "price", "selling price");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    while (r.next()) {
                        System.out.format("\n|%-4d|%-15s|%-12d|%-15.2f|%-15.2f|", r.getInt(1), r.getString(2), r.getInt(3), r.getFloat(4), r.getFloat(5));
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }

            case 2 -> {
                try (PreparedStatement pst = con.prepareCall(query1);) {
                    r = pst.executeQuery();

                    System.err.println("    *************product Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|", "Id", "product name", "quantity", "price", "selling price");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    while (r.next()) {
                        System.out.format("\n|%-4d|%-15s|%-12d|%-15.2f|%-15.2f|", r.getInt(1), r.getString(2), r.getInt(3), r.getFloat(4), r.getFloat(5));
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    };

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            default ->
                System.err.println("enter the correct option");
        }

    }

    public void updateProduct(Connection con) {
        ResultSet r;
        String query = "update  inventory set quantity=? where productid=?";
        while (true) {
            try (PreparedStatement pst = con.prepareCall(query);) {
                System.out.println("enter the product id");
                int id = Integer.parseInt(br.readLine());
                pst.setInt(2, id);

                System.out.println("enter the quantity to change");

                int qnty = Integer.parseInt(br.readLine());
                pst.setInt(1, qnty);
                pst.execute();
                System.out.println("query executed");
                break;

            } catch (IOException ex) {
                System.out.println("exception occurs");
                ex.printStackTrace();
            } catch (NumberFormatException nfe) {
                System.out.println("Numberformat exception occurs");
                System.out.println("enter correct detail");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void placeOrder(Connection con) {
        ResultSet r;
        String query = "select * from cust,katabookorder where cust.cus_id=katabookorder.customer_id";
        String query1 = "select * from cust,katabookorder where cust.cus_id=katabookorder.customer_id and cust.phone=?";
        System.out.println("1.view");
        System.out.println("2.view all");
        System.out.println("enter the option to view");
        int view = 0;
        while (true) {
            try {
                view = Integer.parseInt(br.readLine());
                break;
            } catch (IOException ex) {
                System.out.println("exception occurs place order option");
            } catch (NumberFormatException nfe) {
                System.out.println("Numberformat exception occurs");
                System.out.println("enter correct detail");
            }
        }
        switch (view) {
            case 1 -> {
                try (PreparedStatement pst = con.prepareStatement(query1);) {

                    String phoneNo2 = null;
                    while (true) {
                        try {
                            System.out.println("enter the phone number");
                            phoneNo2 = br.readLine();
                            pst.setString(1, phoneNo2);
                            break;
                        } catch (IOException ex) {
                            System.out.println("exception occurs place order getting phone number");
                        }
                    }

                    r = pst.executeQuery();
                    System.err.println("    *************order Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|", "Id", "name", "phone", "address", "balance", "status", "orderid", "date", "totalAmount");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    while (r.next()) {
                        System.out.format("\n|%-4d|%-15s|%-12s|%-15s|%-15.2f|%-15s|%-15d|%-15s|%-15.2f|", r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getFloat(5), r.getString(6), r.getInt(7), r.getDate(8), r.getFloat(9));
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(KhataBook.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            case 2 -> {
                try (PreparedStatement pst = con.prepareStatement(query);) {
                    r = pst.executeQuery();
                    System.err.println("    *************order Detail************   ");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    System.out.format("\n|%-4s|%-15s|%-12s|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|", "Id", "name", "phone", "address", "balance", "status", "orderid", "date", "totalAmount");
                    System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    while (r.next()) {
                        System.out.format("\n|%-4d|%-15s|%-12s|%-15s|%-15.2f|%-15s|%-15d|%-15s|%-15.2f|", r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getFloat(5), r.getString(6), r.getInt(7), r.getDate(8), r.getFloat(9));
                        System.out.print("\n+" + "-".repeat(4) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(KhataBook.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            default ->
                System.err.println("enter the correct option");
        }
    }

    public void addOrders(Connection con) {
        CreateOrderId c = new CreateOrderId();

        String query = "select * from cust where phone=?";
        ResultSet r;
        try (PreparedStatement pst1 = con.prepareStatement("insert into katabookorder values(?,?,?,?)"); PreparedStatement pst = con.prepareCall(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {

            System.out.println("enter the phone number to add orders");
            String phoneNo2 = null;
            while (true) {
                try {
                    phoneNo2 = br.readLine();
                    pst.setString(1, phoneNo2);
                    r = pst.executeQuery();
                    break;
                } catch (IOException ex) {
                    System.out.println("IOException occurs");

                } catch (NullPointerException npe) {
                    System.out.println("null pointer exception ");
                    System.out.println("enter detai again");
                }
            }
            r.absolute(1);
            int cusId = r.getInt(1);
            pst1.setInt(4, cusId);
            System.out.println(cusId + "cust id");
            int orderId = c.generateOrderId(con);
            pst1.setInt(1, orderId);
            System.out.println(orderId + "orderid");
            Date date = Date.valueOf(LocalDate.now());
            pst1.setDate(2, date);
            double total = 0;
            System.out.println(total + "order total");
            pst1.setDouble(3, total);
            book.addItems(con, orderId, cusId);
             pst1.execute();
            //System.out.println("customer added sucessfully");
            System.out.println("order add sucess fully");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void addItems(Connection con, int order_id, int cusId) {
        UpdateCustomerBalance upb = new UpdateCustomerBalance();
        ResultSet r;
        String query = "Select * from inventory where productid=?";
        double sum = 0;
        LineItems[] l = new LineItems[0];

        boolean add = true;

        boolean productavail = false;
        boolean quantityavail = false;
        CreateOrderMenu m=new CreateOrderMenu();
        m.OrderMenu(con);
        while (add) {
            try (PreparedStatement pst1 = con.prepareStatement("insert into katabooklineitems values(?,?,?,?,?)"); PreparedStatement pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);) {

                l = Arrays.copyOf(l, l.length + 1);
                int orderId = order_id;
                System.out.println(orderId + "line orderid");
                int productId = 0;
                int quantity = 0;
                do {

                    productavail = false;
                    quantityavail = false;

                    System.out.println("enter product id");
                    while (true) {
                        try {
                            productId = Integer.parseInt(br.readLine());
                            pst.setInt(1, productId);
                            r = pst.executeQuery();
                            break;
                        } catch (IOException ex) {
                            System.out.println("IOException occurs");
                            System.out.println("enter detail");
                        } catch (NumberFormatException nfe) {
                            System.out.println("NumberformatException");
                            System.out.println("enter the correct detai");
                        }
                    }

                    if (r.next()) {
                        productavail = true;
                        System.out.println("enter the quantity");
                        while (true) {
                            try {
                                quantity = Integer.parseInt(br.readLine());
                                break;
                            } catch (IOException ex) {
                                System.out.println("IOException occurs");
                                System.out.println("enter detail");
                            } catch (NumberFormatException nfe) {
                                System.out.println("NumberformatException");
                                System.out.println("enter the correct detai");
                            }
                        }
                        int getQuantity = r.getInt(3);
                        if (quantity <= getQuantity) {
                            quantityavail = true;
                        }
                        if (quantityavail == false) {
                            System.out.println("quantity not available");
                            System.out.println("plese enter available quantity");
                        } else {
                            System.out.println("available");
                        }
                    }

                } while (!productavail || !quantityavail);

                double actualPrice = r.getDouble(4);
                double sellingPrice = r.getDouble(5);
                double total = sellingPrice * quantity;
                sum += total;
                double profit = total - (actualPrice * quantity);
                int qn = r.getInt(3) - quantity;
                System.out.println(qn);
                l[l.length - 1] = new LineItems(orderId, productId, quantity, total, profit);
                System.out.println(total + "i item total");
                upb.updateBalance(con, cusId, total);
                r.absolute(1);
                r.updateInt(3, qn);
                r.updateRow();
                //add multiple line items      
                System.out.println("1.add item again");
                System.out.println("2.enough");
                System.out.println("enter the option");
                int op = 0;
                while (true) {
                    try {
                        op = Integer.parseInt(br.readLine());
                        break;
                    } catch (IOException ex) {
                        System.out.println("IOException occurs");
                        System.out.println("enter the detail again");
                    } catch (NumberFormatException nfe) {
                        System.out.println("NumberformatException");
                        System.out.println("enter the correct detai");
                    }
                }
                switch (op) {
                    case 1:
                        add = true;
                        break;
                    case 2:
                        add = false;

                        UpdateOrderTotal upo = new UpdateOrderTotal();
                        for (int i = 0; i < l.length; i++) {
                            int order = l[i].getOrderId();
                            pst1.setInt(1, order);
                            int proid = l[i].getProductId();
                            pst1.setInt(2, proid);
                            int qan = l[i].getQuantity();
                            pst1.setInt(3, qan);
                            double totalprice = l[i].getTotalprice();
                            pst1.setDouble(4, totalprice);
                            double pro = l[i].getProfit();
                            pst1.setDouble(5, pro);
                            pst1.execute();
                            System.out.println(sum + "total+all line items");
                            upo.updatetotal(con, sum, orderId);
                            System.out.println("sucessfully updated customer");
                        }
                        System.out.println("line item added sucessfully");
                        break;
                    default:
                        System.err.println("enter the correct option");

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }

    public void payment(Connection con) {

        UpdateCustomerBalance ucb = new UpdateCustomerBalance();
        CreateOrderId c = new CreateOrderId();
        String query = "select Balance,cus_id from cust where phone=?";
        String insertQuery = "insert into payment values(?,?,?,default)";
        System.out.println("enter the phone number to  payment");
        String phoneNo2 = null;
        double balance = 0;
        int cusId = 0;

        try (PreparedStatement pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); PreparedStatement pst1 = con.prepareStatement(insertQuery);) {
            while (true) {
                try {
                    phoneNo2 = br.readLine();
                    pst.setString(1, phoneNo2);
                    ResultSet r = pst.executeQuery();
                    r.absolute(1);
                    balance = r.getDouble(1);
                    cusId = r.getInt(2);
                    pst1.setInt(2, cusId);
                    System.out.println("your balance:" + balance);
                    break;
                } catch (IOException ex) {
                    System.out.println("IOException occurs");
                    System.out.println("enter the detail");
                }
            }
            System.out.println("enter the payment amount");
            Double pay = Double.parseDouble(br.readLine());
            pst1.setDouble(3, pay);
            int paymentId = c.generatepaymentId(con);
            System.out.println(paymentId + "payment id");
            pst1.setInt(1, paymentId);
            if (pay < balance) {
                String status = "not completed";
                double updateTotal = balance - pay;
                System.out.println(updateTotal + "balance - pay");
                ucb.updatePaymentBalance(con, cusId, updateTotal, status);

            } else if (pay == balance) {
                String status = "completed";
                ucb.updatePaymentBalance(con, cusId, 0, status);

            } else if (pay > balance) {

                System.out.println("enter so much value");
            } else if (balance == 0) {
                System.out.println("no balance in due");

            }
            pst1.execute();

        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }

    }

    public void paymentDetail(Connection con) {
        ResultSet r;
        String query = "select name,phone,adress,balance,status,balancestatus from cust where status='active'";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            r = pst.executeQuery();
          System.err.println("    *************payment Detail************   ");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" );
                    System.out.format("\n|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance", "status","balanceStatus");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+");
                    while (r.next()) {
                        System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|%-15s|%-15s|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4), r.getString(5), r.getString(6));
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" );
                    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void maxbalanceDetail(Connection con) {
        ResultSet r;
        String query = "select name,phone,adress,balance from cust where balance=(select max(balance) from cust)";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            r = pst.executeQuery();
          System.err.println("    *************payment Detail************   ");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) );
                    System.out.format("\n|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance" );
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) );
                    while (r.next()) {
                        System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4));
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) );
                    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
       
    }
    public void minbalanceDetail(Connection con) {
        ResultSet r;
        String query = "select name,phone,adress,balance from cust where balance=(select min(balance) from cust)";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            r = pst.executeQuery();
          System.err.println("    *************payment Detail************   ");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+" );
                    System.out.format("\n|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance" );
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+" );
                    while (r.next()) {
                        System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4));
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+" );
                    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
       
    }
 public void paymentCompleted(Connection con) {
        ResultSet r;
        String query = "select name,phone,adress,balance,balancestatus from cust where balancestatus='completed'";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            r = pst.executeQuery();
          System.err.println("    *************payment Detail************   ");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    System.out.format("\n|%-15s|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance","balancestatus" );
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    while (r.next()) {
                        System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|%-15s|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4),r.getString(5));
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
       
    }
  public void paymentNotCompleted(Connection con) {
        ResultSet r;
        String query = "select name,phone,adress,balance,balancestatus from cust where balancestatus='not_completed'";
        try (PreparedStatement pst = con.prepareStatement(query);) {
            r = pst.executeQuery();
          System.err.println("    *************payment Detail************   ");
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    System.out.format("\n|%-15s|%-15s|%-15s|%-15s|%-15s|", "name", "phone", "address", "balance","balancestatus" );
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    while (r.next()) {
                        System.out.format("\n|%-15s|%-15s|%-15s|%-15.2f|%-15s|", r.getString(1), r.getString(2), r.getString(3), r.getFloat(4),r.getString(5));
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15) + "+" + "-".repeat(15)+"+"+ "-".repeat(15)+"+" );
                    }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
       
    }
    

    public static void main(String[] args) {
        DatabaseConnection1 db = new DatabaseConnection1();

        try (Connection co = db.CreateConnection();) {

            boolean condition = true;
            do {
                System.out.println("1.Customer");
                System.out.println("2.Inventory");
                System.out.println("3.statistics");
                System.out.println("4.Exit");
                System.out.println("enter the option");
                int option = 0;
                while (true) {
                    try {

                        option = Integer.parseInt(br.readLine());

                        break;

                    } catch (NumberFormatException nfe1) {
                        System.out.println("enter the correct option exception");
                    } catch (IOException ex) {
                        System.out.println("IoException occurs");
                    }

                }

                switch (option) {
                    case 1:
                        boolean condition1 = true;
                        do {
                            System.out.println("1.Add Customer");
                            System.out.println("2.Update Customer");
                            System.out.println("3.View customer detail");
                            System.out.println("4.Order placed");
                            System.out.println("5.add orders");
                            System.out.println("6.payment");
                            System.out.println("7.back Exist");
                            System.out.println("enter the Customer option");
                            while (true) {
                                try {
                                    option = Integer.parseInt(br.readLine());
                                    break;

                                } catch (NumberFormatException nfe1) {
                                    System.out.println("enter the correct option exception");
                                } catch (IOException ex) {
                                    System.out.println("Io Exception");
                                }

                            }

                            switch (option) {

                                case 1:

                                    book.addCustomer(co);
                                    break;

                                case 2:
                                    book.updateCustomer(co);
                                    break;

                                case 3:

                                    book.viewCustomer(co);

                                    break;

                                case 4:
                                    book.placeOrder(co);
                                    break;
                                case 5:
                                    book.addOrders(co);
                                    break;
                                case 6:
                                    book.payment(co);
                                    break;
                                case 7:
                                    condition1 = false;
                                    break;
                                default:
                                    System.err.println("enter the correct option");
                                    break;
                            }

                        } while (condition1);
                        break;
                    case 2:
                        boolean condition2 = true;
                        do {
                            System.out.println("1.Add product");
                            System.out.println("2.Update product");
                            System.out.println("3.View product");
                            System.out.println("4.back Exist");
                            System.out.println("enter the Customer option");
                            while (true) {
                                try {
                                    option = Integer.parseInt(br.readLine());
                                    break;

                                } catch (NumberFormatException nfe1) {
                                    System.out.println("enter the correct option io exception occurs");
                                } catch (IOException ex) {
                                    System.out.println("io exception occurs");
                                }

                            }

                            switch (option) {
                                case 1:

                                    book.addProduct(co);
                                    break;

                                case 2:
                                    book.updateProduct(co);
                                    break;

                                case 3:
                                    book.Viewproduct(co);
                                    break;
                                case 4:
                                    condition2 = false;
                                    break;
                                default:
                                    System.out.println("enter the correct option");
                                    break;
                            }

                        } while (condition2);
                        break;
                    case 3:
                        boolean condition3 = true;
                        do {
                            System.out.println("1.Maximum balance");
                            System.out.println("2.min balance");
                            System.out.println("3.payment fully completed");
                            System.out.println("4.payment due customer");
                            System.out.println("5.payment detail");
                            System.out.println("6.exit");
                            System.out.println("enter the Customer option");
                            while (true) {
                                try {
                                    option = Integer.parseInt(br.readLine());
                                    break;

                                } catch (NumberFormatException nfe1) {
                                    System.out.println("enter the correct option exception");
                                } catch (IOException ex) {
                                    System.out.println("io Exception");
                                }

                            }
                            switch (option) {
                                case 1:
                                    book.maxbalanceDetail(co);
                                    break;
                                case 2:
                                    book.minbalanceDetail(co);
                                    break;

                                case 3:
                                    book.paymentCompleted(co);
                                    break;

                                case 4:
                                    book.paymentNotCompleted(co);
                                    break;
                                case 5:
                                    
                                    book.paymentDetail(co);
                                    break;
                                case 6:
                                    condition3 = false;
                                    break;
                                default:
                                    System.out.println("enter the correct option");
                                    break;
                            }

                        } while (condition3);
                        break;
                    case 4:
                        condition = false;
                        break;
                    default:
                        System.out.println("enter the correct option");
                        break;
                }
            } while (condition);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

}
