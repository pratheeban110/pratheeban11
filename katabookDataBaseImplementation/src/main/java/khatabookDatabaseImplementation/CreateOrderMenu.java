/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookDatabaseImplementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bas200175
 */
public class CreateOrderMenu {
    public void OrderMenu(Connection con){
        try {
            String query = "select * from inventory";
            Statement s = con.createStatement();
            ResultSet r;
            r = s.executeQuery(query);
            System.out.printf("%9s%10s%11s%15s%10s", "productID ", " Name ", " Quantity  ", "product_price ");
            System.out.println("");
            while (r.next()) {
                System.out.format("%6s%13s%9s%15s%10s", r.getInt(1), r.getString(2), r.getInt(3), r.getFloat(5));
                System.out.println("");
                System.out.println("------------------------------------------------------------");
            }
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    
    
    }
}
