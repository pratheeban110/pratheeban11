/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookDatabaseImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author bas200175
 */
public class CreateOrderId {
       ResultSet r;
    CheckOrderTable c=new CheckOrderTable();
   public int generateOrderId(Connection con){
   try(PreparedStatement pst3=con.prepareStatement("select * from katabookorder",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);){
     if(!c.Isempty(con)){
        r=pst3.executeQuery();
        r.last();
        return r.getInt(1)+1;
     }
   
   }   catch (SQLException ex) {
          ex.printStackTrace();
       }
   
     
   
     return 1;
   }
   public int generatepaymentId(Connection con){
   try(PreparedStatement pst3=con.prepareStatement("select * from payment",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);){
     if(!c.PaymentIsempty(con)){
        r=pst3.executeQuery();
        r.last();
        return r.getInt(1)+1;
     }
   
   }   catch (SQLException ex) {
          ex.printStackTrace();
       }
   
     
   
     return 1;
   }
}
