package khatabookDatabaseImplementation;

import java.util.Arrays;
import java.util.Scanner;
public class Product {

    Scanner scn = new Scanner(System.in);

    private int producId;
    private String productName;
    private double productPrice;
    private double productSellingPrice;
    private int productQuantity;
    

    public Product() {
        super();
    }

    public Product(int producId, String productName, double productPrice,double productSellingPrice, int productQuantity) {
        this.producId = producId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSellingPrice=productSellingPrice;
        this.productQuantity = productQuantity;
    }

    public void setProducId(int producId) {
        this.producId = producId;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public int getProducId() {
        return producId;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public double getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(double productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }
    
     
    @Override
    public String toString() {
        return "product id:" + producId + "\n" + "product name:" 
                + productName + "\n" + "product price:" + productSellingPrice 
                + "\n" + "product Qnty:" + productQuantity;
    }
   

}
