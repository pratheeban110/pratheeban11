/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookDatabaseImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bas200175
 */
public class UpdateCustomerBalance {
    ResultSet r;
    public void updateBalance(Connection con,int cusId,double total){
        try(PreparedStatement  pst = con.prepareStatement("select * from cust where cus_id=?",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);) {
            pst.setInt(1, cusId);
            r=pst.executeQuery();
            r.absolute(1);
            double actualBalance=r.getDouble(5);
            r.updateDouble(5, actualBalance+total);
            r.updateRow();
            System.out.println("update customer balance sucessfully");
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    }
    public void updatePaymentBalance(Connection con,int cusId,double total,String status){
        try(PreparedStatement  pst = con.prepareStatement("select * from cust where cus_id=?",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);) {
            pst.setInt(1, cusId);
            r=pst.executeQuery();
            r.absolute(1);
            double actualBalance=r.getDouble(5);
            r.updateDouble(5, total);
            r.updateString(7, status);
            r.updateRow();
            System.out.println("update customer balance sucessfully");
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    }
}
