 package khatabookDatabaseImplementation;

import java.time.LocalDate;
import java.util.Scanner;

public class Orders {
    private int orderId;
    private int cusId;
    private LocalDate date;
    private double orderAmount;

    Orders[] o = new Orders[0];

    public Orders(int orderId, int cusId, LocalDate date,double orderAmount) {
        this.orderId = orderId;
        this.cusId = cusId;
        this.date = date;
        this.orderAmount=orderAmount;
    }

    

    public Orders() {
        super();
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getCusId() {
        return cusId;
    }
  

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

 
    
    

    @Override
    public String toString() {
        return  ":orderId :  "  + orderId+"\n" + "cutomer ID  :  " 
                + cusId  + "\n" +"Date  :   "+ date
                +"\n"+"Total Amount:"+orderAmount;
    }

}
